package mova.groovy;

import groovy.lang.GroovyRuntimeException;

public class GroovyValidationException extends GroovyRuntimeException {

    public GroovyValidationException() {}

    public GroovyValidationException(String message) {
        super(message);
    }

    public GroovyValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroovyValidationException(Throwable t) {
        super(t);
    }
}
