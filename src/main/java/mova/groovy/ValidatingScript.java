package mova.groovy;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

import java.util.Map;

public abstract class ValidatingScript<K, V> extends Script {

    private static final GroovyShell GROOVY_SHELL = new GroovyShell();

    private final String source;
    private final Script script;

    protected ValidatingScript(String source) {
        this.source = source;
        this.script = GROOVY_SHELL.parse(source);
    }

    @Override
    public Object run() {
        return script.run();
    }

    @Override
    public Binding getBinding() {
        return script.getBinding();
    }

    @Override
    public void setBinding(Binding binding) {
        script.setBinding(binding);
    }

    @Override
    public Object getProperty(String property) {
        return script.getProperty(property);
    }

    @Override
    public void setProperty(String property, Object newValue) {
        script.setProperty(property, newValue);
    }

    protected abstract void validate(Map<K, V> context) throws GroovyValidationException;

    public String getSource() {
        return source;
    }

    public Script getScript() {
        return script;
    }
}
