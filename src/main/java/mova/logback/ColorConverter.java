package mova.logback;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.pattern.CompositeConverter;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiElement;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.boot.ansi.AnsiStyle;

import java.util.*;

public class ColorConverter extends CompositeConverter<ILoggingEvent> {

    private static final Map<String, List<AnsiElement>> ELEMENTS;

    static {
        Map<String, List<AnsiElement>> ansiElements = new HashMap<>();
        ansiElements.put("faint", Arrays.asList(AnsiColor.WHITE, AnsiStyle.FAINT));
        ansiElements.put("white", Collections.singletonList(AnsiColor.WHITE));
        ansiElements.put("red", Collections.singletonList(AnsiColor.RED));
        ansiElements.put("green", Collections.singletonList(AnsiColor.GREEN));
        ansiElements.put("yellow", Collections.singletonList(AnsiColor.YELLOW));
        ansiElements.put("blue", Collections.singletonList(AnsiColor.BLUE));
        ansiElements.put("magenta", Collections.singletonList(AnsiColor.MAGENTA));
        ansiElements.put("cyan", Collections.singletonList(AnsiColor.CYAN));
        ELEMENTS = Collections.unmodifiableMap(ansiElements);
    }

    private static final Map<Integer, List<AnsiElement>> LEVELS;

    static {
        Map<Integer, List<AnsiElement>> ansiLevels = new HashMap<>();
        ansiLevels.put(Level.ERROR_INTEGER, Arrays.asList(AnsiColor.RED, AnsiStyle.BOLD));
        ansiLevels.put(Level.WARN_INTEGER, Arrays.asList(AnsiColor.YELLOW, AnsiStyle.BOLD));
        ansiLevels.put(Level.INFO_INTEGER, Collections.singletonList(AnsiColor.DEFAULT));
        ansiLevels.put(Level.DEBUG_INTEGER, Collections.singletonList(AnsiColor.WHITE));
        ansiLevels.put(Level.TRACE_INTEGER, Arrays.asList(AnsiColor.WHITE, AnsiStyle.ITALIC));
        LEVELS = Collections.unmodifiableMap(ansiLevels);
    }

    @Override
    protected String transform(ILoggingEvent event, String in) {
        List<AnsiElement> elements = ELEMENTS.get(getFirstOption());
        if (elements == null) {
            elements = LEVELS.get(event.getLevel().toInteger());
            elements = (elements != null) ? elements : Collections.singletonList(AnsiColor.WHITE);
        }
        return toAnsiString(in, elements);
    }

    private String toAnsiString(String in, List<AnsiElement> elements) {
        List<Object> objects = new ArrayList<>(elements);
        objects.add(in);
        return AnsiOutput.toString(objects.toArray());
    }

}