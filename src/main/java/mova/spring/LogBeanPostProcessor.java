package mova.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class LogBeanPostProcessor implements BeanPostProcessor {

    private final Logger logger = LoggerFactory.getLogger(LogBeanPostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        String beanClassName = bean.getClass().toString();
        if (beanClassName.contains("mova")) {
            logger.debug("Bean instantiated with name {} and class {}", beanName, bean.getClass());
        }

        return bean;
    }
}