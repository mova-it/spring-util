package mova.spring.debug;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
@Profile("dev")
@ConditionalOnProperty(value = "mova.properties.show", havingValue = "true", matchIfMissing = true)
public class AppContextRefreshedEventPropertiesPrinter {

    private final Logger logger = LoggerFactory.getLogger(AppContextRefreshedEventPropertiesPrinter.class);

    @EventListener
    public void handleContextRefreshed(ContextRefreshedEvent event) {
        if (logger.isDebugEnabled()) {
            ConfigurableEnvironment env = (ConfigurableEnvironment) event.getApplicationContext().getEnvironment();

            String info = env.getPropertySources()
                    .stream()
                    .filter(MapPropertySource.class::isInstance)
                    .map(ps -> ((MapPropertySource) ps).getSource().keySet())
                    .flatMap(Collection::stream)
                    .distinct()
                    .sorted()
                    .map(key -> String.format("%s=%s", key, env.getProperty(key)))
                    .collect(Collectors.joining("\n"));

            logger.debug("Properties list: \n{}",info);
        }
    }
}
