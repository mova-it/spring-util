package mova.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

@Component
public class JsonManagerImpl implements JsonManager {

    @JsonPropertyOrder({"message", "cause", "suppressed"})
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonIgnoreProperties({"stackTrace", "localizedMessage", "mostSpecificCause"})
    public abstract static class ExceptionMixIn extends Exception {}

    private final ObjectMapper mapper = new ObjectMapper().addMixIn(Exception.class, ExceptionMixIn.class);

    @Override
    public <T> T unmap(String json, Class<T> clazz) throws IOException {
        return mapper.readValue(json, clazz);
    }

    @Override
    public <T> T unmap(String json, TypeReference<T> valueTypeRef) throws IOException {
        return mapper.readValue(json, valueTypeRef);
    }

    @Override
    public <T> T unmap(InputStream jsonStream0, Class<T> clazz) throws IOException {
        try (InputStream jsonStream = jsonStream0) {
            return mapper.readValue(jsonStream, clazz);
        }
    }

    @Override
    public <T> List<T> unmapList(String json, Class<T> clazz) throws IOException {
        CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
        return mapper.readValue(json, javaType);
    }

    @Override
    public <T> List<T> unmapList(InputStream jsonStream0, Class<T> clazz) throws IOException {
        CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
        try (InputStream jsonStream = jsonStream0) {
            return mapper.readValue(jsonStream, javaType);
        }
    }

    @Override
    public <T> List<T> unmapNestedList(String json, String nestedListName, Class<T> clazz) throws IOException {
        JsonNode nestedList = mapper.readTree(json).path(nestedListName);
        CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
        return mapper.convertValue(nestedList, javaType);
    }

    @Override
    public String map(Object o) throws IOException {
        return map(mapper.writer(), false, null, o);
    }

    @Override
    public String map(Object o, Class<?> view) throws IOException {
        return map(mapper.writer(), false, view, o);
    }

    @Override
    public String prettyMap(Object o) throws IOException {
        return map(mapper.writer(), true, null, o);
    }

    @Override
    public String prettyMap(Object o, Class<?> view) throws IOException {
        return map(mapper.writer(), true, view, o);
    }

    @Override
    public void mapTo(Object o, OutputStream outputStream) throws IOException {
        mapTo(mapper.writer(), false, null, o, outputStream);
    }

    @Override
    public void mapTo(Object o, Class<?> view, OutputStream outputStream) throws IOException {
        mapTo(mapper.writer(), false, view, o, outputStream);
    }

    @Override
    public void prettyMapTo(Object o, OutputStream outputStream) throws IOException {
        mapTo(mapper.writer(), true, null, o, outputStream);
    }

    @Override
    public void prettyMapTo(Object o, Class<?> view, OutputStream outputStream) throws IOException {
        mapTo(mapper.writer(), true, view, o, outputStream);
    }

    private String map(ObjectWriter writer, boolean pretty, Class<?> view, Object o) throws JsonProcessingException {
        if (pretty) writer = writer.withDefaultPrettyPrinter();
        if (view != null) writer = writer.withView(view);

        return writer.writeValueAsString(o);
    }

    private void mapTo(ObjectWriter writer, boolean pretty, Class<?> view, Object o, OutputStream outputStream) throws IOException {
        if (pretty) writer = writer.withDefaultPrettyPrinter();
        if (view != null) writer = writer.withView(view);

        writer.writeValue(outputStream, o);
    }
}
