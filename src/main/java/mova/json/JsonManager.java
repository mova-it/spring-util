package mova.json;

import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface JsonManager {

    <T> T unmap(String json, Class<T> clazz) throws IOException;

    <T> T unmap(String json, TypeReference<T> valueTypeRef) throws IOException;

    <T> T unmap(InputStream jsonStream, Class<T> clazz) throws IOException;

    <T> List<T> unmapList(String json, Class<T> clazz) throws IOException;

    <T> List<T> unmapList(InputStream jsonStream, Class<T> clazz) throws IOException;

    <T> List<T> unmapNestedList(String json, String nestedListName, Class<T> clazz) throws IOException;

    String map(Object o) throws IOException;

    String map(Object o, Class<?> view) throws IOException;

    String prettyMap(Object o) throws IOException;

    String prettyMap(Object o, Class<?> view) throws IOException;

    void mapTo(Object o, OutputStream outputStream) throws IOException;

    void mapTo(Object o, Class<?> view, OutputStream outputStream) throws IOException;

    void prettyMapTo(Object o, OutputStream outputStream) throws IOException;

    void prettyMapTo(Object o, Class<?> view, OutputStream outputStream) throws IOException;
}
