package mova.jpa;

import org.hibernate.NaturalIdLoadAccess;
import org.hibernate.Session;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@Transactional(readOnly = true)
public class NaturalIdRepositoryImpl<T, I> extends SimpleJpaRepository<T, I> implements NaturalIdRepository<T, I> {

    private final EntityManager entityManager;

    public NaturalIdRepositoryImpl(JpaEntityInformation<T, I> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);

        this.entityManager = entityManager;
    }

    @Override
    public boolean existsBySimpleNaturalId(Serializable naturalId) {
        return existsBySimpleNaturalId(naturalId, false);
    }

    @Override
    public boolean existsBySimpleNaturalId(Serializable naturalId, boolean synchronize) {
        return findBySimpleNaturalId(naturalId, synchronize).isPresent();
    }

    @Override
    public Optional<T> findBySimpleNaturalId(Serializable naturalId) {
        return findBySimpleNaturalId(naturalId, false);
    }

    @Override
    public Optional<T> findBySimpleNaturalId(Serializable naturalId, boolean synchronize) {
        try (Session session = entityManager.unwrap(Session.class)) {
            return session.bySimpleNaturalId(getDomainClass())
                    .setSynchronizationEnabled(synchronize)
                    .loadOptional(naturalId);
        }
    }

    @Override
    public boolean existsByNaturalId(Map<String, Object> naturalIds) {
        return existsByNaturalId(naturalIds, false);
    }

    @Override
    public boolean existsByNaturalId(Map<String, Object> naturalIds, boolean synchronize) {
        return findByNaturalId(naturalIds, synchronize).isPresent();
    }

    @Override
    public Optional<T> findByNaturalId(Map<String, Object> naturalIds) {
        return findByNaturalId(naturalIds, false);
    }

    @Override
    public Optional<T> findByNaturalId(Map<String, Object> naturalIds, boolean synchronize) {
        try (Session session = entityManager.unwrap(Session.class)) {
            NaturalIdLoadAccess<T> loadAccess = session.byNaturalId(getDomainClass());
            naturalIds.forEach(loadAccess::using);

            return loadAccess
                    .setSynchronizationEnabled(synchronize)
                    .loadOptional();
        }
    }

}