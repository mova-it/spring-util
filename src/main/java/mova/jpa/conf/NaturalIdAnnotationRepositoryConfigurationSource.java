package mova.jpa.conf;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.data.repository.config.AnnotationRepositoryConfigurationSource;
import org.springframework.data.util.Streamable;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.lang.annotation.Annotation;
import java.util.*;

//TODO peut être relire l'ensemble pour amélioration
public class NaturalIdAnnotationRepositoryConfigurationSource extends AnnotationRepositoryConfigurationSource {

    private static final String BASE_PACKAGES = "basePackages";
    private static final String BASE_PACKAGE_CLASSES = "basePackageClasses";

    private final AnnotationMetadata configMetadata;
    private final Environment environment;

    NaturalIdAnnotationRepositoryConfigurationSource(AnnotationMetadata metadata, Class<? extends Annotation> annotation, ResourceLoader resourceLoader, Environment environment,BeanDefinitionRegistry registry, @Nullable BeanNameGenerator generator) {
        super(metadata, annotation, resourceLoader, environment, registry, generator);

        Assert.notNull(metadata, "Metadata must not be null!");
        Assert.notNull(resourceLoader, "ResourceLoader must not be null!");

        this.configMetadata = metadata;
        this.environment = environment;
    }

    @Override
    public Streamable<String> getBasePackages() {
        String[] value = getAttributes().getStringArray("value");
        String[] basePackages = getAttributes().getStringArray(BASE_PACKAGES);
        Class<?>[] basePackageClasses = getAttributes().getClassArray(BASE_PACKAGE_CLASSES);

        // Default configuration - return package of annotated class
        if (value.length == 0 && basePackages.length == 0 && basePackageClasses.length == 0) {

            String className = configMetadata.getClassName();
            return Streamable.of(ClassUtils.getPackageName(className));
        }

        List<String> packagesFromValue = parsePackagesSpel(value);
        List<String> packagesFromBasePackages = parsePackagesSpel(basePackages);

        Set<String> packages = new HashSet<>();
        packages.addAll(packagesFromValue);
        packages.addAll(packagesFromBasePackages);

        for (Class<?> typeName : basePackageClasses) {
            packages.add(ClassUtils.getPackageName(typeName));
        }

        return Streamable.of(packages);
    }

    private List<String> parsePackagesSpel(String[] raws) {
        List<String> results = new ArrayList<>();
        for (String raw : raws) {
            raw = raw.trim();

            if (!raw.startsWith("$")) {
                if (StringUtils.hasText(raw)) continue;

                results.addAll(Arrays.asList(raw.split(",")));
            } else {
                String packages = environment.getProperty(raw.substring("${".length(), raw.length() - "}".length()));

                if (StringUtils.hasText(packages)) results.addAll(Arrays.asList(packages.split(",")));
                else results.add(raw);
            }
        }

        return results;
    }
}