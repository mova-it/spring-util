package mova.jpa;

import mova.jpa.conf.NaturalIdEnableJpaRepositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
@ConditionalOnProperty("mova.jpa.natural-id.base-packages")
@NaturalIdEnableJpaRepositories(basePackages = "${mova.jpa.natural-id.base-packages}", repositoryBaseClass = NaturalIdRepositoryImpl.class)
public class NaturalIdAutoconfiguration {


    @Value("${mova.jpa.natural-id.base-packages}")
    private String[] basePackages;

    @PostConstruct
    private void init() {
        Logger logger = LoggerFactory.getLogger(NaturalIdAutoconfiguration.class);
        if (logger.isDebugEnabled()) {
            logger.debug("Configuration du NaturalIdRepository pour le package {}", Arrays.toString(basePackages));
        }
    }
}
