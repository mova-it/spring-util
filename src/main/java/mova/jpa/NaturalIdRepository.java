package mova.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;

@NoRepositoryBean
public interface NaturalIdRepository<T, I> extends JpaRepository<T, I> {

    // use this method when your entity has a single field annotated with @NaturalId
    boolean existsBySimpleNaturalId(Serializable naturalId);
    boolean existsBySimpleNaturalId(Serializable naturalId, boolean synchronize);
    Optional<T> findBySimpleNaturalId(Serializable naturalId);
    Optional<T> findBySimpleNaturalId(Serializable naturalId, boolean synchronize);

    // use this method when your entity has more than one field annotated with @NaturalId
    boolean existsByNaturalId(Map<String, Object> naturalIds);
    boolean existsByNaturalId(Map<String, Object> naturalIds, boolean synchronize);
    Optional<T> findByNaturalId(Map<String, Object> naturalIds);
    Optional<T> findByNaturalId(Map<String, Object> naturalIds, boolean synchronize);
}
